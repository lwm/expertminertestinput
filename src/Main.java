import java.util.ArrayList;
import java.util.List;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Hallo Welt!");
		
		final List<Data> data = new ArrayList<Data>();
		for(int i = 0; i < 42; i++) {
			Data d = new Data(String.format("Data %d", i));
			data.add(d);
		}
		test1((byte)42, 1337);
		test2();
	}
	
	private static int get(int[] a, int i) {
		return a[i];
	}
	
	private static void test1(byte param1, int param2) {
		int[] a = new int[100];
		int[] b = new int[100];
		int[] c = new int[100];
		int[] result = new int[100];
		
		for(int i = 0; i < 51; i++) {
			result[i] = get(a, i + 1) + get(a, i - 1) * param1;
		}
		
		for(int i = 0; i < 51; i++) {
			result[i] = get(a, i + 1) + get(b, i - 1) * param2;
		}
		
		for(int i = 0; i < 51; i++) {
			result[i] = get(a, i + 1) + get(c, i - 1) * Math.max(param1, param2);
		}
	}
	
	private static void test2() {
		int[] x = new int[20];
		int[] y = new int[20];
		for(int i = 0; i < x.length; i++) {
			y[i] = x[i] * 2;
		}
	}
}
