
public class BaseClass {
	
	private final int number;
	private static int count = 0;
	
	public BaseClass() {
		this.number = count++;
	}
	
	public int getNumber() {
		return number;
	}
}
