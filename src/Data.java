
public class Data extends BaseClass {
	
	private String text;	
	
	public Data(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	@Override
	public String toString() {
		return String.format("(%d) %s", this.getNumber(), text);
	}
}
